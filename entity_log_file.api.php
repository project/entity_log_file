<?php

/**
 * @file
 * Hooks provided by the Date module.
 */

/**
 * Add a log rotation scheme for the file-based entity log
 *
 * @return array $rotation
 *   A keyed array in which each key corresponds to a rotation scheme.
 *   Each scheme is an array with the following information:
 *   - label: Label, which will be displayed on the admin form 
 *     (required)
 *   - description: String which will be appended to the rotation 
 *     option description (required)
 *   - form_callback: This is a callback to a function that takes in the
 *     admin form, and returns an updated form. This is used to add 
 *     additional form fields to the global admin form. (optional)
 *   - name_callback: Callback which generates a log filename from the 
 *     entity information. (required)
 *     @see hook_entity_insert
 *     @see hook_entity_update
 *     @see hook_entity_delete
 *   - cron_callback: Callback which called during hook_cron, to handle
 *     cleanup of log files (optional)
 * 
 *   Note: if you do not wish to use an optional vaule, set it to NULL.
 */

function hook_entity_log_file_log_rotate() {
  $rotation['manual'] = array(
    'label' => t('Manual/external rotation'),
    'description' => t('Rotation will be handled manaully or with a utility like logrotate. 
    Do not enable on production sites without rotation.'),
    'form_callback' => NULL,
    'name_callback' => 'entity_log_file_rotate_manual_name',
    'cron_callback' => NULL,
  );
  $rotation['daily'] = array(
    'label' => t('Rotate Daily'),
    'description' => t('Rotates logs once a day, giving each log a datestamp. 
    Deleted after a specified time period.'),
    'form_callback' => 'entity_log_file_rotate_weekly_form',
    'name_callback' => 'entity_log_file_rotate_weekly_name',
    'cron_callback' => 'entity_log_file_rotate_weekly_crontask',
  );
  return $rotation;
}

/**
 * Add a data formatting scheme for the File-Based Entity Log.
 *
 * @return array $formats
 *   A keyed array in which each key corresponds to a format scheme.
 *   Each scheme is an array with the following information:
 *   - label: Label, which will be displayed on the admin form 
 *     (required)
 *   - description: String which will be appended to the format 
 *     option description (required)
 *   - callback: callback which takes in formatted write data (keyed 
 *     array), and returns a string to write to the log. Do not add
 *     PHP_EOL in this function, it will be added for you.
 * 
 *   Note: if you do not wish to use an optional vaule, set it to NULL.
 */

function hook_entity_log_file_log_format() {
  $formats['json'] = array(
    'label' => t('JSON Format'),
    'description' => t('JSON encodes each line as a JSON object.'),
    'callback' => 'entity_log_file_log_json',
  );
  return $formats;
}
