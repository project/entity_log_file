CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

File-Based Entity Log is a module geared to record major entity events (create,
update, delete) in a file-based log, as opposed to a database.

The primary use case is to act as a data source for intermediate backups (i.e.
the time between scheduled DB backups), though one can use the output as source
data for other languages and applications, depending on your use case.


REQUIREMENTS
------------

  This module has no requirements in terms of toher Drupal modules. However,
if you elect to use "manual rotation," you may want to use a log rotation
program such as logrotate on your server. Otherwise, the log may become
unmaintainably large over time.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.


CONFIGURATION
-------------

Todo: write the configration section

Hooks
-----
Todo: write the hooks section
